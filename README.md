# BoredAPI test automation

This code is several automated BoredAPI testcases. It is made on JS/TS using Jest. The tests are made using promises and async/await patterns.

---

## Installing and running

1. npm install
2. npm test

---

## Tests description

Here are several suites with tests

1. Randomization check: 
	- check using promises, 
	- check using async/await
2. Price check
	- Price filting
	- Price range filtering
3. Key check
	- Valid key check
	- Invalid key check
4. Type check
	- Valid type check
	- Invalid type check
5. Participants check
6. Accessibility check
	- Accessibility filtering
	- Accessibility range filtering
---

## Additional info

BorderAPI is wrapped by a class which return a promise on GET request.
Major constants are located in a dedicated file.