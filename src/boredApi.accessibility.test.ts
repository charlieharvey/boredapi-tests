import { BoredAPI } from "./boredApi";

describe('Accessibility filtering tests', () => {

	const boredApi = new BoredAPI();

	test('Filter by accessibility', () => {
		let accessibilityValue: number = 0.05;
		// Promise
		return boredApi.get({ 'accessibility': accessibilityValue }).then(data => {
			expect(data.accessibility).toBe(accessibilityValue);
		});
	});

	test('Filter by accessibility range', async () => {
		let minAccessibilityValue: number = 0.05;
		let maxAccessibilityValue: number = 0.15
		// Async/await
		let data = await boredApi.get({ 'minaccessibility': minAccessibilityValue, "maxaccessibility": maxAccessibilityValue });

		expect(data.accessibility).toBeGreaterThanOrEqual(minAccessibilityValue);
		expect(data.accessibility).toBeLessThanOrEqual(maxAccessibilityValue);
	});
});