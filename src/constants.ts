export enum TYPES {
	Education = "education",
	Recreational = "recreational",
	Social = "social",
	Diy = "diy",
	Charity = "charity",
	Cooking = "cooking",
	Relaxation = "relaxation",
	Music = "music",
	Busywork = "busywork"
}

export const VALID_KEY: number = 6613330

export interface ActivityResponseType {
	activity?: string;
	type?: TYPES;
	participants?: number;
	price?: number;
	link?: string;
	key?: string;
	accessibility?: number;
}
export const VALID_ACTIVITY_RESPONSE: ActivityResponseType = {
	"activity": "Pot some plants and put them around your house",
	"type": TYPES.Relaxation,
	"participants": 1,
	"price": 0.4,
	"link": "",
	"key": "6613330",
	"accessibility": 0.3
}

export const INVALID_KEY: number = 999

export interface ErrorResponseType {
	error: string;
}
export const ERROR_RESPONSE: ErrorResponseType = {
	error: 'No activity found with the specified parameters'
}
