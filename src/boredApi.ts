import * as http from 'http';
import { ActivityResponseType } from './constants';

export class BoredAPI {
	baseUrl: string;
	defaultRoute: string;

	constructor() {
		this.baseUrl = 'http://www.boredapi.com';
		this.defaultRoute = "/api/activity/";
	}

	get(params: object): Promise<ActivityResponseType> {
		return new Promise((resolve, _) => {
			let path: string = this.configurePath_(params);
			http.get(this.baseUrl, { path: path }, res => {
				let data = [];

				res.on('data', chunk => {
					data.push(chunk);
				});

				res.on('end', () => {
					resolve(JSON.parse(Buffer.concat(data).toString()));
				});
			}).on('error', err => {
				console.log('Error: ', err.message);
			});
		});
	}

	configurePath_(params: object): string {
		let path: string = this.defaultRoute;
		if (params) {
			path += "?";
			for (const [key, value] of Object.entries(params)) {
				path += "" + key + "=" + value + "&";
			}
		}
		return path;
	}
}
