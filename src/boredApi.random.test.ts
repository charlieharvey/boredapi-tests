import { BoredAPI } from "./boredApi";

describe('Randomization check tests', () => {

	const boredApi = new BoredAPI();

	test('Check randomization using promices', () => {
		let dataPromise1 = boredApi.get(null);
		let dataPromise2 = boredApi.get(null);

		return Promise.all([dataPromise1, dataPromise2]).then(values => {
			expect(values[0]).not.toEqual(values[1]);
		});
	});

	test('Check randomization using async/await', async () => {

		let data1 = await boredApi.get(null);
		let data2 = await boredApi.get(null);

		expect(data1).not.toEqual(data2);
	});
});