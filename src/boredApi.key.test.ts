import { BoredAPI } from "./boredApi";
import { VALID_KEY, VALID_ACTIVITY_RESPONSE, INVALID_KEY, ERROR_RESPONSE } from "./constants";

describe('Key filtering tests', () => {

	const boredApi = new BoredAPI();

	test('Filter by valid key', () => {
		// Promise
		return boredApi.get({ 'key': VALID_KEY }).then(data => {
			expect(data).toStrictEqual(VALID_ACTIVITY_RESPONSE);
		});
	});

	test('Filter by invalid key', async () => {
		// Async/await
		let data = await boredApi.get({ 'key': INVALID_KEY })
		expect(data).toStrictEqual(ERROR_RESPONSE);
	});
});