import { BoredAPI } from "./boredApi";

describe('Participants filtering tests', () => {

	const boredApi = new BoredAPI();

	test('Filter by participants', async () => {
		let participantsValue: number = 2;

		let data = await boredApi.get({ 'participants': participantsValue });
		expect(data.participants).toBe(participantsValue);

	});
});