import { BoredAPI } from "./boredApi";
import { TYPES, ERROR_RESPONSE } from "./constants";

describe('Type filtering tests', () => {

	const boredApi = new BoredAPI();

	test('Filter by valid type', () => {
		// Promise
		return boredApi.get({ 'type': TYPES.Diy }).then(data => {
			expect(data.type).toBe(TYPES.Diy);
		});
	});

	test('Filter by invalid type', async () => {
		// Async/await
		let data = await boredApi.get({ 'type': 'invalid_value' });
		expect(data).toStrictEqual(ERROR_RESPONSE);
	});
});
