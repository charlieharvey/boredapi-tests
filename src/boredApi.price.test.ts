import { BoredAPI } from "./boredApi";

describe('Price filtering tests', () => {

	const boredApi = new BoredAPI();

	test('Filter by price ', () => {
		let priceValue: number = 0.15;
		// Promise 
		return boredApi.get({ 'price': priceValue }).then(data => {
			expect(data.price).toBe(priceValue);
		});
	});

	test('Filter by price range ', async () => {

		let minPriceValue: number = 0.05;
		let maxPriceValue: number = 0.15

		// Async/await
		const data = await boredApi.get({ 'minprice': minPriceValue, "maxprice": maxPriceValue });
		expect(data.price).toBeGreaterThanOrEqual(minPriceValue);
		expect(data.price).toBeLessThanOrEqual(maxPriceValue);
	});
});